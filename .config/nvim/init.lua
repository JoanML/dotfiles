require('plugins')

local map = vim.api.nvim_set_keymap

vim.g.mapleader = ","

vim.o.completeopt = "menuone,noselect"

vim.o.wildmenu = true
vim.o.virtualedit = "all"
vim.o.smartcase = true
vim.o.cmdheight = 2
vim.o.showcmd = true
vim.o.showmatch = true
vim.o.encoding = "utf-8"
vim.o.number = true -- line numbers
vim.o.hlsearch = true
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.termguicolors = true
vim.o.background = "light"
vim.o.magic = true

vim.wo.foldcolumn = "1" -- margin on the left

vim.bo.expandtab = true
vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.autoindent = true
vim.bo.smartindent = true

map('n', '<silent><expr> <C-Space>', 'compe#complete()', {noremap = true})
map('n', '<silent><expr> <CR>', 'compe#confirm("<CR>")', {noremap = true})
map('n', '<silent><expr> <C-e>', 'compe#close("<C-e>")', {noremap = true})
map('n', '<silent><expr> <C-f>', 'compe#scroll({ "delta": +4 })', {noremap = true})
map('n', '<silent><expr> <C-d>', 'compe#scroll({ "delta": -4 })', {noremap = true})

map('n', '<leader>rs', ':ReplToggle<CR>', {noremap = true})
map('n', '<leader>rr', '<Plug>ReplSendLine', {noremap = false})
map('v', '<leader>rr', '<Plug>ReplSendVisual', {noremap = false})

map('n', '<leader>d', ':nohlsearch<CR>', {noremap = true})
map('n', '0', '^', {})
map('n', '<space>', 'za', {noremap = true})

map('n', '<leader>p', '"0p"', {noremap = false})
map('n', '<leader>P', '"0P"', {noremap = false})

map('n', '<leader>ww', ':vertical new<cr>', {noremap = true})
map('n', '<leader>wq', ':20new<cr>', {noremap = true})
map('n', '<leader>t', ':terminal<cr>', {noremap = true})
map('t', '<C-W>', '<C-\\><C-n>', {noremap = true})

map('n', '<leader>b', '<:bnext<CR>', {noremap = true})

map('i', '<C-C>', '<Esc>', {noremap = true})

map('', '<C-j>', '<C-W>j', {noremap = false})
map('', '<C-k>', '<C-W>k', {noremap = false})
map('', '<C-h>', '<C-W>h', {noremap = false})
map('', '<C-l>', '<C-W>l', {noremap = false})

map('n', '<leader>ff', '<cmd>lua require("telescope.builtin").find_files()<cr>', {noremap = true})
map('n', '<leader>fg', '<cmd>lua require("telescope.builtin").live_grep()<cr>', {noremap = true})
map('n', '<leader>fa', '<cmd>lua require("telescope.builtin").lsp_code_actions()<cr>', {noremap = true})
map('n', '<leader>fb', '<cmd>lua require("telescope.builtin").buffers()<cr>', {noremap = true})
map('n', '<leader>fh', '<cmd>lua require("telescope.builtin").help_tags()<cr>', {noremap = true})

vim.cmd([[
" " Delete trailing white space on save
" fun! CleanExtraSpaces()
"     let save_cursor = getpos(".")
"     let old_query = getreg('/')
"     silent! %s/\s\+$//e
"     call setpos('.', save_cursor)
"     call setreg('/', old_query)
" endfun
"
" if has("autocmd")
"     autocmd BufWritePre * :call CleanExtraSpaces()
" endif

autocmd BufWritePre * :%s/\s\+$//e

" Return to last edit position when opening files
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Normal tabs in text files
autocmd BufEnter *.txt,*.csv,*.tsv set expandtab!

augroup templates
	autocmd BufNewFile *.sh 0r ~/.config/nvim/templates/bash.sh
	autocmd BufNewFile *.qsub 0r ~/.config/nvim/templates/qsub.qsub
augroup END
]])

vim.o.background = "light"
vim.cmd([[colorscheme gruvbox]])
