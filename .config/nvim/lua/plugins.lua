require('packer').startup(function()
  use 'wbthomason/packer.nvim'

  use 'neovim/nvim-lspconfig'
  use 'williamboman/nvim-lsp-installer'

  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-nvim-lsp'
  use "hrsh7th/cmp-nvim-lsp-signature-help"

  use 'hrsh7th/vim-vsnip'
  use 'hrsh7th/cmp-vsnip'

  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }

  use 'JoosepAlviste/nvim-ts-context-commentstring'
  use 'numToStr/Comment.nvim'

  use 'tpope/vim-repeat'
  use 'pappasam/nvim-repl'

  use 'kyazdani42/nvim-web-devicons'
  use 'kyazdani42/nvim-tree.lua'

  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  use 'nvim-lua/lsp-status.nvim'
  use 'hoob3rt/lualine.nvim'

  use {
    'romgrk/barbar.nvim',
    requires = {'kyazdani42/nvim-web-devicons'}
  }

  use { 'ellisonleao/gruvbox.nvim' }

  use 'sunjon/Shade.nvim'

  use 'norcalli/nvim-colorizer.lua'

end)

require'nvim-treesitter.configs'.setup {
    ensure_installed = "maintained",
    highlight = {
        enable = true,
    },
    incremental_selection = {
        enable = true
    },
    indent = {
        enable = false
    },
    context_commentstring = {
	enable = true,
	enable_autocmd = false,
    }
}

vim.cmd([[
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
]])

-- LSPConfig
--

local lsp_installer = require("nvim-lsp-installer")
lsp_installer.on_server_ready(function(server)
    local opts = { }
    server:setup(opts)
    vim.cmd([[ do User LspAttach Buffers ]])
end)


-- COMPLETION
local cmp = require'cmp'
cmp.setup({
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
    end,
  },
  mapping = {
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'nvim_lsp_signature_help' },
    { name = 'vsnip' }, -- For vsnip users.
    { name = 'path' },
    { name = 'buffer', keyword_length = 5},
  }),
  menu = {
    buffer = "[buf]",
    nvim_lsp = "[LSP]",
    path = "[path]",
    vsnip = "[snip]",
  },
  experimental = {
    native_menu = false,
    ghost_text = true,
  },
})

require('Comment').setup {
  pre_hook = function(ctx)
    local U = require 'Comment.utils'

    local location = nil
    if ctx.ctype == U.ctype.block then
      location = require('ts_context_commentstring.utils').get_cursor_location()
    elseif ctx.cmotion == U.cmotion.v or ctx.cmotion == U.cmotion.V then
      location = require('ts_context_commentstring.utils').get_visual_start_location()
    end

    return require('ts_context_commentstring.internal').calculate_commentstring {
      key = ctx.ctype == U.ctype.line and '__default' or '__multiline',
      location = location,
    }
  end,
}

require'lualine'.setup {
  options = {
    icons_enabled = true,
    theme = 'gruvbox',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff',
                  {"require'lsp-status'.status()"}},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
}

vim.api.nvim_set_var('repl_filetype_commands', {
  python = 'ipython',
  r = 'R',
  rmd = 'R'
})

vim.api.nvim_set_var('nvim_tree_icons', {
  default = '',
  symlink = '',
  git = {
    unstaged = "✗",
    staged = "✓",
    unmerged = "",
    renamed = "➜",
    untracked = "★",
    deleted = "",
    ignored = "◌"
    },
  folder = {
    arrow_open = "",
    arrow_closed = "",
    default = "",
    open = "",
    empty = "",
    empty_open = "",
    symlink = "",
    symlink_open = "",
    },
    lsp = {
      hint = "",
      info = "",
      warning = "",
      error = "",
    }
})

require'nvim-tree'.setup {
    open_on_setup = true,
    open_on_tab = true,
    auto_close = true,
    view = {
        width = 40,
        height = 40,
        hide_root_folder = false,
        side = 'left',
        auto_resize = true,
        mappings = {
          custom_only = false,
          list = {}
        }
    }
}

require'shade'.setup({
  overlay_opacity = 50,
  opacity_step = 1,
})

require('telescope').setup{
    defaults = {
	  mappings = {
	      i = {
	          ["<C-j>"] = "move_selection_next",
	          ["<C-k>"] = "move_selection_previous",
	      }
	  }
    }
}
